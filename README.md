For various reasons, some software undergo a transition and subsequently refer to themselves using a new name. The list below includes some useful software and their deadname(s). If anyone claims to represent a project under one of the following deadname(s), they are definitely not genuine and are likely trying to scam or misguide you. The project's official website is also provided below. If you know any software that underwent a transition that isn't already a list, please submit a PR or open an issue. 

| Name | Deadname(s) |
| ---  | ---         |
| [Forgejo](https://forgejo.org) | Gitea, Gogs |
| [GrapheneOS](https://grapheneos.org) | CopperheadOS |
| [Indigenous Peoples' Day](https://en.wikipedia.org/wiki/Indigenous_Peoples'_Day_(United_States)) | Columbus Day |
| [Libera.Chat](https://libera.chat) | Freenode |
| [LineageOS](https://lineageos.org) | CyanogenMod |
| [OpenSearch](https://opensearch.org) | ElasticSearch |
| [OpenTofu](https://opentofu.org) | OpenTF, Terraform |
| [Resonite](https://resonite.com) | Neos VR |
| [Strawberry](https://strawberrymusicplayer.org) | Clementine |
| [Tenacity](https://tenacityaudio.org) | Audacity |

